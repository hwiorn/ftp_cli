

# Simple ftp downloader

## Install
`pip3 install --upgrade git+https://hwiorn@bitbucket.org/hwiorn/ftp_cli.git`

or

`sudo pip3 install --upgrade git+https://hwiorn@bitbucket.org/hwiorn/ftp_cli.git`

## Options
```
usage: ftp_cli.py [-h] [-q] [-c HOST] [-P PORT] [-u USER] [-p PASSWORD]
                  [-l LIST] [-d DOWNLOAD] [--cut-dirs CUT_DIRS]

FTP download client

optional arguments:
  -h, --help            show this help message and exit
  -q, --silent          Do not print any messages (default: False)
  -c HOST, --host HOST  Host ip (default: None)
  -P PORT, --port PORT  Port (default: 21)
  -u USER, --user USER  Username (default: anonymous)
  -p PASSWORD, --password PASSWORD
                        Password (default: )
  -l LIST, --list LIST  List path (default: None)
  -d DOWNLOAD, --download DOWNLOAD
                        Download directories or file (default: None)
  --cut-dirs CUT_DIRS   Cut directories (default: 0)
```

## Usages
### List the files
```
$ ftpcli -c <someip> -P 21 -u <guest> -p <guest_password> -l /public/data/
```

### Download the file
```
$ ftpcli -c <someip> -P 21 -u <guest> -p <guest_password> -d /public/data/somefile.txt
```

### Download the directories
```
$ ftpcli -c <someip> -P 21 -u <guest> -p <guest_password> -d /public/data/
```
