#!/usr/bin/env python3
import argparse
import os.path
import socket
from ftplib import FTP

parser = argparse.ArgumentParser(
    description='FTP download client',
    formatter_class=argparse.ArgumentDefaultsHelpFormatter)
parser.add_argument(
    '-q', '--silent', help='Do not print any messages', action='store_true')
parser.add_argument('-c', '--host', help='Host ip')
parser.add_argument('-P', '--port', help='Port', type=int, default=21)
parser.add_argument('-u', '--user', help='Username', default='anonymous')
parser.add_argument('-p', '--password', help='Password', default='')
parser.add_argument('-l', '--list', help='List path')
parser.add_argument('-d', '--download', help='Download directories or file')
parser.add_argument('--cut-dirs', help='Cut directories', type=int, default=0)
args = parser.parse_args()


class DownloadProgress:
    def __init__(self, filepath, size=0):
        self.filename = os.path.basename(filepath)
        self.size = size
        self.inc = 0
        self.file = open(filepath, 'wb')

    def write(self, data):
        datalen = len(data)
        self.file.write(data)
        self.inc += datalen
        if not args.silent:
            print(
                '> %s Bytes: %d of %d (%2.3f%%) %s' %
                (self.filename, self.inc, self.size,
                 self.inc / self.size * 100., self.makePrettySize(self.size)),
                end="\r")

        if self.inc >= self.size:
            if not args.silent:
                print()

            self.file.close()

    def makePrettySize(self, size):
        for unit in ['B', 'KB', 'MB', 'GB', 'TB', 'PB']:
            if size < 1024:
                return '{:.1f}{}'.format(size, unit)
            size /= 1024


class ZFTP(FTP):
    def __init__(self, host, port=21, user='', pw=''):
        FTP.__init__(self)
        self.host = host
        self.port = port
        self.user = user
        self.pw = pw
        #self.set_debuglevel(2)
        self.connect(self.host, self.port)
        self.login(self.user, self.pw)

    def cd(self, path):
        self.cwd(path)

    def isdir(self, path):
        orig_path = self.pwd()
        try:
            try:
                self.cwd(path)
            except Exception as e:
                if "550" in str(e):
                    return False

            return True
        finally:
            self.cwd(orig_path)

    def ls(self, path=None):
        try:
            orig_path = self.pwd()
            if path:
                self.cwd(path)

            return self.nlst()

        finally:
            self.cwd(orig_path)

    def filesize(self, path):
        try:
            self.sendcmd('TYPE i')
            return self.size(path)
        finally:
            self.sendcmd('TYPE A')

    def downfile(self, remote_path, local_path):
        orig_path = self.pwd()
        try:
            if not args.silent:
                print('Download: {} -> {}'.format(remote_path, local_path))

            rpath = os.path.dirname(remote_path)
            filename = os.path.basename(remote_path)
            filesize = self.filesize(remote_path)

            self.cwd(rpath)
            self.retrbinary("RETR " + filename,
                                DownloadProgress(local_path, filesize).write)
        finally:
            self.cwd(orig_path)

    def downall(self, path, cut_depth=0):
        for f in self.ls(path):
            cpath = os.path.join(path, f)
            lpath = os.sep.join(
                os.path.normpath(cpath).split(os.sep)[cut_depth:])
            if self.isdir(cpath):
                if not os.path.exists(lpath):
                    os.makedirs(lpath)

                self.downall(cpath, cut_depth)
            else:
                self.downfile(cpath, lpath)

    # https://stackoverflow.com/q/22991417, Overwrite the pvsv ip address
    def makepasv(self):
        host, port = super().makepasv()
        if self.af == socket.AF_INET:
            host = self.host
            #print('227')
        else:
            #print('229')
            pass

        return host, port


def main():
    ftp = ZFTP(args.host, args.port, args.user, args.password)
    if args.list:
        for f in ftp.ls(args.list):
            if ftp.isdir(args.list + '/' + f):
                print('[{}]'.format(f))
            else:
                print(f)

    if args.download:
        if args.download[:1] == '/':
            args.download = args.download[1:]

        ftp.downall(args.download, args.cut_dirs)

    ftp.quit()


if __name__ == '__main__':
    main()
