import os
from setuptools import setup, find_packages
from distutils.version import StrictVersion


def read(fname: str) -> str:
    return open(
        os.path.join(os.path.dirname(__file__), fname),
        encoding='utf-8').read()


version = '1.0.0'
StrictVersion(version)

dependencies = ['argparse']

setup(
    name='ftp_cli',
    version=version,
    description='FTP client to download the files and the directories',
    long_description=read('README.md'),
    license='GPLv3+',
    author='hwiorn',
    author_email='hwiorn@mail.com',
    keywords=['ftp'],
    url='https://bitbucket.org/hwiorn/ftp_cli',
    download_url='https://bitbucket.org/hwiorn/ftp_cli/downloads/' + version,
    zip_safe=False,
    packages=find_packages(exclude=['tests']),
    test_suite='tests.get_suite',
    scripts=['ftp_cli.py'],
    entry_points={
        'console_scripts': ['ftp_cli = ftp_cli:main', 'ftpcli = ftp_cli:main']
    },
    install_requires=dependencies,
    classifiers=[
        'Environment :: Console',
        'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)',
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.2',
        'Programming Language :: Python :: 3.3',
        'Programming Language :: Python :: 3.4',
        'Programming Language :: Python :: 3.5',
        'Programming Language :: Python :: 3 :: Only',
        'Development Status :: 4 - Beta',
        'Topic :: System :: Archiving :: Backup',
        'Topic :: System :: Filesystems'
    ])
